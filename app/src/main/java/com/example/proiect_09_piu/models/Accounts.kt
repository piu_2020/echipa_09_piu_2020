package com.example.proiect_09_piu.models

import java.io.Serializable

class Accounts : Serializable {

    private var accounts : ArrayList<Account> = ArrayList<Account>()

    constructor(){
        this.accounts.add(Account("Rares","Popa","rares","rares@gmail.com","admin"))
        this.accounts.add(Account("Emanuel","Golban","emanuel","emanuel@gmail.com","admin"))
        this.accounts.add(Account("Grigor","Ipatiov","grigor","grigor@gmail.com","admin"))
        this.accounts.add(Account("Laura","Mindrutiu","laura","laura@gmail.com","admin"))
    }

    fun addAccount(account:Account){
        this.accounts.add(account)
    }

    fun getAccounts() : ArrayList<Account>{
        return this.accounts
    }

}