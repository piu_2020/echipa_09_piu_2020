package com.example.proiect_09_piu.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.proiect_09_piu.MainActivity
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.models.Account
import com.example.proiect_09_piu.models.Accounts

class RegisterActivity : AppCompatActivity() {

    private var lastNameInput : Boolean = false
    private var firstNameInput : Boolean = false
    private var usernameInput : Boolean = false
    private var emailInput : Boolean = false
    private var passwordMatch : Boolean = false


    private var passwordTouched : Boolean = false
    private var passwordConfirmTouched : Boolean = false

    private lateinit var lastName : EditText
    private lateinit var firstName : EditText
    private lateinit var username : EditText
    private lateinit var email : EditText
    private lateinit var password : EditText
    private lateinit var passwordConfirm : EditText

    private lateinit var accounts : Accounts

    private fun checkForm() : Boolean{
        return lastNameInput && firstNameInput && usernameInput && emailInput && passwordMatch
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        accounts = intent!!.getSerializableExtra("accounts") as Accounts

        username = findViewById(R.id.register_username_editText)
        firstName = findViewById(R.id.register_firstName_editText)
        lastName = findViewById(R.id.register_lastName_editText)
        email = findViewById(R.id.register_email_editText)
        password = findViewById(R.id.register_password_editText)
        passwordConfirm = findViewById(R.id.register_passwordConfirm_editText)


        username!!.doOnTextChanged { text, _, _, _ ->
            usernameInput = text!!.length >= 3
            if(usernameInput)
                findViewById<TextView>(R.id.register_username_error_message).visibility = View.GONE
            else
                findViewById<TextView>(R.id.register_username_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        firstName!!.doOnTextChanged { text, _, _, _ ->
            firstNameInput = text!!.length >= 3
            if(firstNameInput)
                findViewById<TextView>(R.id.register_firstName_error_message).visibility = View.GONE
            else
                findViewById<TextView>(R.id.register_firstName_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        lastName!!.doOnTextChanged { text, _, _, _ ->
            lastNameInput = text!!.length >= 3
            if(lastNameInput)
                findViewById<TextView>(R.id.register_lastName_error_message).visibility = View.GONE
            else
                findViewById<TextView>(R.id.register_lastName_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        email!!.doOnTextChanged { text, _, _, _ ->
            emailInput = android.util.Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()
            if(emailInput)
                findViewById<TextView>(R.id.register_email_error_message).visibility = View.GONE
            else
                findViewById<TextView>(R.id.register_email_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        password!!.doOnTextChanged { text, _, _, _ ->
            passwordTouched = true
            if(passwordConfirmTouched && (text.toString() == passwordConfirm.text.toString())) {
                findViewById<TextView>(R.id.register_password_error_message).visibility = View.GONE
                passwordMatch = true
            }
            else if(passwordConfirmTouched)
                findViewById<TextView>(R.id.register_password_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        passwordConfirm!!.doOnTextChanged { text, _, _, _ ->
            passwordConfirmTouched = true
            if(passwordTouched && (text.toString() == password.text.toString())) {
                findViewById<TextView>(R.id.register_password_error_message).visibility = View.GONE
                passwordMatch = true
            }
            else if(passwordTouched)
                findViewById<TextView>(R.id.register_password_error_message).visibility = View.VISIBLE
            findViewById<Button>(R.id.register_button).isEnabled = checkForm()
        }

        findViewById<Button>(R.id.register_button).setOnClickListener{register(it)}

    }

    private fun register(it: View?) {

        val firstNameString = firstName.text.toString()
        val lastNameString = lastName.text.toString()
        val usernameString = username.text.toString()
        val emailString = email.text.toString()
        val passwordString = password.text.toString()

        val account = Account(firstNameString,lastNameString,usernameString,emailString,passwordString)

        accounts.addAccount(account)

        val intent = Intent(this,MainActivity::class.java).apply {
            putExtra("accounts",accounts)
        }
        startActivity(intent)

    }

}