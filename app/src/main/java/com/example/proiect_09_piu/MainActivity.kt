package com.example.proiect_09_piu

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.proiect_09_piu.activities.RegisterActivity
import com.example.proiect_09_piu.models.Accounts
import com.example.proiect_09_piu.models.LoggedUser

class MainActivity : AppCompatActivity() {

    private var isInputUsername : Boolean = false
    private var isInputPassword : Boolean = false

    private lateinit var username : EditText
    private lateinit var password : EditText

    private lateinit var accounts : Accounts

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(intent != null && intent.getSerializableExtra("accounts") != null)  {
            accounts = intent.getSerializableExtra("accounts") as Accounts
        }else{
            accounts = Accounts()
        }

        findViewById<TextView>(R.id.login_register_link).paintFlags = Paint.UNDERLINE_TEXT_FLAG

        findViewById<TextView>(R.id.login_register_link).setOnClickListener{register(it)}

        username = findViewById(R.id.login_username_editText)
        password = findViewById(R.id.login_password_editText)

        username!!.doOnTextChanged { text, _, _, _ ->
            isInputUsername = !text.isNullOrEmpty()
            findViewById<Button>(R.id.login_button).isEnabled = isInputPassword.and(isInputUsername)
        }
        password!!.doOnTextChanged { text, _, _, _ ->
            isInputPassword = !text.isNullOrEmpty()
            findViewById<Button>(R.id.login_button).isEnabled = isInputPassword.and(isInputUsername)
        }
        findViewById<Button>(R.id.login_button).setOnClickListener{login(it)}
    }

    private fun register(it: View?) {
        val intent = Intent(this,RegisterActivity::class.java).apply {
            putExtra("accounts",accounts)
        }
        startActivity(intent)
    }

    private fun checkCredentials(username:String,password:String) : Boolean{
        for(acc in accounts.getAccounts()){
            if(acc.username == username && acc.password == password)
                return true
        }
        return false
    }

    private fun login(it: View?) {
        val success = checkCredentials(username.text.toString(),password.text.toString())
        if(success){
            LoggedUser.username = username.text.toString()
            LoggedUser.password = password.text.toString()
            //TODO : change activity or fragment to dashboard
            username.setText("")
            password.setText("")
            username.clearFocus()
            password.clearFocus()

            val intent = Intent(this,NavigationDrawerActivity::class.java)
            startActivity(intent)
        }else{
            findViewById<TextView>(R.id.login_error_message).visibility = View.VISIBLE
        }
    }

}