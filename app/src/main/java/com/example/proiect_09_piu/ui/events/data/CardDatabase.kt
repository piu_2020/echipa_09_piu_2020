package com.example.proiect_09_piu.ui.events.data

import com.example.proiect_09_piu.R

class CardDatabase {

    var events: ArrayList<CardData>
    var news: ArrayList<CardData>

    init {
        events = ArrayList<CardData>()
        events.add(CardData(R.drawable.festival_image, "Incepe TIFF. Afla programul acum", "12/06/2019"))
        events.add(CardData(R.drawable.tiff1_image, "Filmul \"Morometii 2\" a fost premiat", "13/06/2019"))
        events.add(CardData(R.drawable.tiff2_image, "TIFF 2019 a luat sfarsit", "14/06/2019"))

        news = ArrayList<CardData>()
        news.add(CardData(R.drawable.bus_image, "O noua statie pe strada Ferdinand", "12/06/2019",false))
        news.add(CardData(R.drawable.street_image, "Probleme de infrastructura pe strada Traian", "13/06/2019",false))
        news.add(CardData(R.drawable.boc_iscusitul, "Locuri de parcare monitorizate", "14/06/2019",false))
    }


}