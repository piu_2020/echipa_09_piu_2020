package com.example.proiect_09_piu.ui.review.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.data.Review

class ReviewAdapter(private val context: Context, private var dataList: ArrayList<Review>) : RecyclerView.Adapter<ReviewViewHolder>() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val view = inflater.inflate(R.layout.review_card_item,parent,false)
        return ReviewViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bindData(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun changeList(newList : ArrayList<Review>){
        this.dataList = newList
    }

    fun getCompany(index:Int) : Review {
        return dataList[index]
    }
}