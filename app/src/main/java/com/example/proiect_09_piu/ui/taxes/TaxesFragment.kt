package com.example.proiect_09_piu.ui.taxes

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment

import com.example.proiect_09_piu.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TaxesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TaxesFragment : Fragment() {
    private lateinit var textTaxNumber: TextInputEditText
    private lateinit var textType: AutoCompleteTextView
    private lateinit var textPaySum: TextInputEditText

    private lateinit var taxNumberLayout: TextInputLayout
    private lateinit var typeLayout: TextInputLayout
    private lateinit var paySumLayout: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_taxes, container, false)

        val itemsTip = listOf("Taxa Salubritate", "Amenda", "Taxa Anuala parcare")

        val dropDownTip = view.findViewById<AutoCompleteTextView>(R.id.taxes_dropdown_tip_text)

        val adapterTip = ArrayAdapter(requireContext(), R.layout.dropdown_item, itemsTip)

        dropDownTip?.setAdapter(adapterTip)

        textTaxNumber = view.findViewById(R.id.taxes_textField_factura)
        textType = view.findViewById(R.id.taxes_dropdown_tip_text)
        textPaySum = view.findViewById(R.id.taxes_textField_plata)

        taxNumberLayout = view.findViewById(R.id.taxes_textField_factura_layout)
        typeLayout = view.findViewById(R.id.taxes_dropdown_tip)
        paySumLayout = view.findViewById(R.id.taxes_textField_plata_layout)

        textTaxNumber.doOnTextChanged { text, start, before, count ->  taxNumberLayout.isErrorEnabled = false }
        textType.doOnTextChanged { text, start, before, count ->  typeLayout.isErrorEnabled = false }
        textPaySum.doOnTextChanged { text, start, before, count ->  paySumLayout.isErrorEnabled = false }

        val button: Button = view.findViewById(R.id.taxes_payButton) as Button
        button.setOnClickListener(View.OnClickListener {
            handlePressButton(view)
        })

        val buttonQR: FloatingActionButton = view.findViewById(R.id.taxes_qr_button) as FloatingActionButton
        buttonQR.setOnClickListener(View.OnClickListener {
            textTaxNumber.setText("19856G25")
            textType.setText("Taxa Salubritate")
            textPaySum.setText("25.50")
        })

        return view
    }

    fun validateInput (view: View): Boolean {
        var valid = true;
        val textTaxNumber = getView()?.findViewById<TextInputEditText>(R.id.taxes_textField_factura)
        val textType = getView()?.findViewById<AutoCompleteTextView>(R.id.taxes_dropdown_tip_text)
        val textPaySum = getView()?.findViewById<TextInputEditText>(R.id.taxes_textField_plata)

        if (textTaxNumber?.text.isNullOrBlank()) {
            taxNumberLayout.isErrorEnabled = true
            taxNumberLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        if (textType?.text.isNullOrBlank()) {
            typeLayout.isErrorEnabled = true
            typeLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        if (textPaySum?.text.isNullOrBlank()) {
            paySumLayout.isErrorEnabled = true
            paySumLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        if (!textPaySum?.text.toString().matches(Regex("\\d+(?:\\.\\d+)?"))) {
            paySumLayout.isErrorEnabled = true
            paySumLayout.error = "Suma introdusa nu este valida"
            valid = false
        }

        return valid;
    }

    fun handlePressButton(view: View) {
        if (validateInput(view)) {
            //pentru prezentare
            //val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.maiestiprost.com"))
            //pentru cand se uita Domnul Profesor de la curs
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plataonline.com"))
            startActivity(browserIntent)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TaxesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TaxesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}