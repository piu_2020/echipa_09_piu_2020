package com.example.proiect_09_piu.ui.appointments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class AppointmentsFragment : Fragment() {
    private lateinit var ghiseu: AutoCompleteTextView
    private lateinit var data_programare: AutoCompleteTextView
    private lateinit var CNP: TextInputEditText
    private lateinit var ora: AutoCompleteTextView
    private lateinit var submitButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_appointments, container, false)
        ghiseu = view.findViewById(R.id.appointment_category)
        data_programare = view.findViewById(R.id.appointment_date)
        CNP = view.findViewById(R.id.appointment_CNP)
        ora = view.findViewById(R.id.appointment_hours)
        submitButton = view.findViewById(R.id.appointment_button)
        data_programare.setText("ZZ/LL/AAAA")

        val layoutCategory = view.findViewById(R.id.appointments_textfield) as TextInputLayout;
        val layoutDate = view.findViewById(R.id.appointments_textfield2) as TextInputLayout;
        val layoutCNP = view.findViewById(R.id.appointments_textfield3) as TextInputLayout;
        val layoutHour = view.findViewById(R.id.appointments_textfield4) as TextInputLayout;

        ghiseu.setOnClickListener {
            layoutCategory.isErrorEnabled = false
        }

        data_programare.setOnClickListener {
            layoutDate.isErrorEnabled = false
        }
        CNP.setOnClickListener {
            layoutCNP.isErrorEnabled = false
        }
        ora.setOnClickListener {
            layoutHour.isErrorEnabled = false
        }


        val items = listOf("Evidenta Populatiei", "Audienta primar", "Abonamente parcari")
        var adapter = ArrayAdapter(requireContext(), R.layout.list_item_1, items)
        ghiseu.setAdapter(adapter)


        ghiseu.doOnTextChanged { text, _, _, _ ->
            when {
                ghiseu.text.toString() == "Evidenta Populatiei" -> {
                    val dates = listOf("10/01/2021", "11/01/2021", "13/01/2021")
                    var adapter2 = ArrayAdapter(requireContext(), R.layout.list_item_1, dates)
                    data_programare.setAdapter(adapter2)

                    val hours = listOf("08:00", "08:30", "10:00")
                    var adapter3 = ArrayAdapter(requireContext(), R.layout.list_item_1, hours)
                    ora.setAdapter(adapter3)
                }
                ghiseu.text.toString() == "Audienta primar" -> {
                    val dates = listOf("15/01/2021", "16/01/2021", "17/01/2021")
                    var adapter2 = ArrayAdapter(requireContext(), R.layout.list_item_1, dates)
                    data_programare.setAdapter(adapter2)

                    val hours = listOf("08:45", "09:30", "10:30")
                    var adapter3 = ArrayAdapter(requireContext(), R.layout.list_item_1, hours)
                    ora.setAdapter(adapter3)
                }
                ghiseu.text.toString() == "Abonamente parcari" -> {
                    val dates = listOf("10/02/2021", "11/02/2021", "13/02/2021")
                    var adapter2 = ArrayAdapter(requireContext(), R.layout.list_item_1, dates)
                    data_programare.setAdapter(adapter2)

                    val hours = listOf("12:00", "13:00", "13:30", "14:00")
                    var adapter3 = ArrayAdapter(requireContext(), R.layout.list_item_1, hours)
                    ora.setAdapter(adapter3)
                }

            }
        }


        submitButton.setOnClickListener {

            if (ghiseu.text.toString().isEmpty()) {
                layoutCategory.isErrorEnabled = true
                layoutCategory.error = "Alegeti o categorie!"
            }
            if (data_programare.text.toString().isEmpty()) {
                layoutDate.isErrorEnabled = true
                layoutDate.error = "Alegeti o data!"
            }
            if (ora.text.toString().isEmpty()) {
                layoutHour.isErrorEnabled = true
                layoutHour.error = "Alegeti o ora!"
            }
            if (CNP.text.toString().length != 13) {
                layoutCNP.isErrorEnabled = true
                layoutCNP.error = "CNP invalid!"
            }
            if (!ghiseu.text.toString().isEmpty() && !data_programare.text.toString()
                    .isEmpty() && !ora.text.toString().isEmpty() && CNP.text.toString().length == 13
            ) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage("Programarea a fost inregistrata!")
                    .setPositiveButton("OK") { dialog, which ->
                        ghiseu.setText("")
                        data_programare.setText("")
                        CNP.setText("")
                        ora.setText("")
                    }
                    .show()
            }

        }

        return view
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            AppointmentsFragment()

    }
}