package com.example.proiect_09_piu.ui.theatre

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.theatre.data.CardDetails

import com.google.android.material.textfield.TextInputLayout

class PlayDetailsFragment : Fragment() {
    private lateinit var textOre: AutoCompleteTextView
    private lateinit var textLocuri: AutoCompleteTextView

    private lateinit var oreLayout: TextInputLayout
    private lateinit var locuriLayout: TextInputLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_play_details, container, false)!!

        val title = CardDetails.currentSelection?.title!!
        val date = CardDetails.currentSelection?.date!!

        view.findViewById<TextView>(R.id.play_details_title).text = title
        view.findViewById<TextView>(R.id.play_details_date).text = date

        val itemsOre = listOf("16:00", "19:00")
        val itemsLocuri = listOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")

        val dropDownOre = view.findViewById<AutoCompleteTextView>(R.id.play_dropdown_hours_text)
        val dropDownLocuri = view.findViewById<AutoCompleteTextView>(R.id.play_dropdown_seats_text)

        val adapterOre = ArrayAdapter(requireContext(), R.layout.dropdown_item, itemsOre)
        val adapterLocuri = ArrayAdapter(requireContext(), R.layout.dropdown_item, itemsLocuri)

        dropDownOre.setAdapter(adapterOre)
        dropDownLocuri.setAdapter(adapterLocuri)

        textOre = view.findViewById(R.id.play_dropdown_hours_text)
        textLocuri = view.findViewById(R.id.play_dropdown_seats_text)

        oreLayout = view.findViewById(R.id.play_dropdown_hours)
        locuriLayout = view.findViewById(R.id.play_dropdown_seats)

        textOre.doOnTextChanged { text, start, before, count -> oreLayout.isErrorEnabled = false }
        textLocuri.doOnTextChanged { text, start, before, count -> locuriLayout.isErrorEnabled = false }

        val button: Button = view.findViewById(R.id.play_payButton) as Button
        button.setOnClickListener(View.OnClickListener {
            handlePressButton(view)
        })

        return view
    }

    fun validateInput(view: View): Boolean {
        var valid = true
        val textOre = view.findViewById<AutoCompleteTextView>(R.id.play_dropdown_hours_text)
        val textLocuri = view.findViewById<AutoCompleteTextView>(R.id.play_dropdown_seats_text)

        if (textOre.text.isNullOrBlank()) {
            oreLayout.isErrorEnabled = true
            oreLayout.error = "Acest camp este obligatoriu!"
            valid = false
        }

        if (textLocuri.text.isNullOrBlank()) {
            locuriLayout.isErrorEnabled = true
            locuriLayout.error = "Acest camp este obligatoriu!"
            valid = false
        }

        return valid
    }

    fun handlePressButton(view: View) {
        if (validateInput(view)) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plataonline.com"))
            startActivity(browserIntent)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PlayDetailsFragment()
    }
}