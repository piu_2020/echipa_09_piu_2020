package com.example.proiect_09_piu.ui.live

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.models.LoggedUser
import com.example.proiect_09_piu.ui.live.adapter.ChatAdapter
import com.example.proiect_09_piu.ui.live.messages.Message
import com.example.proiect_09_piu.ui.live.messages.Messages
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.schedule

class LiveFragment : Fragment() {

    private var chatAdapter: ChatAdapter? = null
    private lateinit var sendButton: ImageView;
    private lateinit var messageField: EditText;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_live, container, false)

        val recyclerRef = view.findViewById(R.id.chat_messages_rv) as RecyclerView;
        val linearLayoutManager = LinearLayoutManager(view.context)
        recyclerRef.layoutManager = linearLayoutManager

        sendButton = view.findViewById(R.id.send_button)
        chatAdapter = ChatAdapter(view.context, Messages().getMessages())
        recyclerRef.adapter = chatAdapter
        messageField = view.findViewById(R.id.chat_message_field)
        val sdf = SimpleDateFormat("hh:mm")
        sendButton.setOnClickListener {
            if (messageField.text.isNotEmpty()) {
                val message = Message(LoggedUser.username, sdf.format(Date()), messageField.text.toString())
                messageField.setText("")
                chatAdapter!!.addItem(chatAdapter!!.itemCount, message)
                chatAdapter!!.notifyItemInserted(chatAdapter!!.itemCount)
                recyclerRef.smoothScrollToPosition(chatAdapter!!.itemCount)
            }
        }
        val randomMessages = listOf("felicitari", "sa va fie rusine", "nu sunt de acord", "era de asteptat")
        val randomUsers = listOf("Maria", "Laura", "Robert", "Mihai", "Marcel", "Alex", "Flavia", "Razvan")
        val timer = object: CountDownTimer(200000, (1000..4000).shuffled().first().toLong()) {
            override fun onTick(millisUntilFinished: Long) {
                chatAdapter!!.addItem(chatAdapter!!.itemCount, Message(randomUsers.random(), sdf.format(Date()), randomMessages.random()))
                chatAdapter!!.notifyItemInserted(chatAdapter!!.itemCount)
                recyclerRef.smoothScrollToPosition(chatAdapter!!.itemCount)
            }

            override fun onFinish() {
            }

        }
        timer.start();

        return view
    }


}