package com.example.proiect_09_piu.ui.incidents

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.ProgressBar
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.concurrent.schedule

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class IncidentFragment : Fragment() {

    private lateinit var categoryField: AutoCompleteTextView;
    private lateinit var descriptionField: TextInputEditText;
    private lateinit var locationField: TextInputEditText;
    private lateinit var submitButton: Button;
    private lateinit var spinner: ProgressBar;

    private lateinit var categoryLayout: TextInputLayout;
    private lateinit var descriptionLayout: TextInputLayout;
    private lateinit var locationLayout: TextInputLayout;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_incident, container, false)

        categoryField = view.findViewById(R.id.incident_category_field)
        descriptionField = view.findViewById(R.id.incident_input_field)
        locationField = view.findViewById(R.id.incident_location_input_field)
        submitButton = view.findViewById(R.id.submit_incident_button)
        spinner = view.findViewById(R.id.action_bar_spinner)

        categoryLayout = view.findViewById(R.id.incident_category_dropdown)
        categoryField.doOnTextChanged { text, start, before, count ->  categoryLayout.isErrorEnabled = false;}


        descriptionLayout = view.findViewById(R.id.incident_input_layout)
        descriptionField.doOnTextChanged { text, start, before, count ->  descriptionLayout.isErrorEnabled = false;}


        locationLayout = view.findViewById(R.id.incident_location_input_layout)
        locationField.doOnTextChanged { text, start, before, count ->  locationLayout.isErrorEnabled = false;}


        val items = listOf("Infrastructura", "Transport public", "Salubritare", "Spatii publice")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
        categoryField.setAdapter(adapter)

        val alertDialog = AlertDialog.Builder(this.context)
        alertDialog.setTitle("Incidentul a fost raportat cu succes!")
        alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))

        submitButton.setOnClickListener {
            if(validateInputs(categoryField, descriptionField,locationField)) {
                spinner.visibility = View.VISIBLE
                Timer("Spinner", false).schedule(2000) {
                    Handler(Looper.getMainLooper()).post {
                        spinner.visibility = View.GONE
                        alertDialog.show()
                    }
                }
            }
        }

        return view
    }

    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
            categoryField.setText("");
            descriptionField.setText("");
            locationField.setText("");
    }
    fun validateInputs(categoryField: AutoCompleteTextView,
                       descriptionField: TextInputEditText,
                        locationField: TextInputEditText) : Boolean {

        var flag = true;
        if (categoryField.text.toString().isEmpty()) {
            categoryLayout.isErrorEnabled = true
            categoryLayout.error = "Acest camp este obligatoriu!"
            flag = false;
        }
        if (descriptionField.text.toString().isEmpty()) {
            descriptionLayout.isErrorEnabled = true
            descriptionLayout.error = "Acest camp este obligatoriu!"
            flag = false;
        }
        if (locationField.text.toString().isEmpty()) {
            locationLayout.isErrorEnabled = true
            locationLayout.error = "Acest camp este obligatoriu!"
            flag = false;
        }

        return flag;

    }

}