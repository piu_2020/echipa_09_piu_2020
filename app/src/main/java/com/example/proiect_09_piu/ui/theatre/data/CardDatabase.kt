package com.example.proiect_09_piu.ui.theatre.data

import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.theatre.data.CardData

class CardDatabase {

    var plays: ArrayList<CardData>

    init {
        plays = ArrayList<CardData>()
        plays.add(CardData(R.drawable.hamlet, "Hamlet", "12/06/2019"))
        plays.add(CardData(R.drawable.donjuan, "Don Juan", "13/06/2019"))
        plays.add(CardData(R.drawable.angry12, "12 Oameni furiosi", "14/06/2019"))
    }


}