package com.example.proiect_09_piu.ui.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.adapter.ReviewDetailsAdapter
import com.example.proiect_09_piu.ui.review.data.ReviewSelection
import com.example.proiect_09_piu.ui.review.data.UserReviews
import com.example.proiect_09_piu.ui.review.dialog.ReviewDialog


class ReviewDetailsFragment : Fragment() {

    private lateinit var recyclerRef: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_review_details, container, false)!!

        val title = ReviewSelection.current?.title!!
        val rating = ReviewSelection.current?.rating!!
        val image = ReviewSelection.current?.image!!
        val list = UserReviews.hashMap[title]!!

        recyclerRef = view.findViewById(R.id.review_details_recycler);
        recyclerRef.layoutManager = LinearLayoutManager(view.context);
        recyclerRef.adapter = ReviewDetailsAdapter(view.context, list);

        view.findViewById<ImageView>(R.id.review_details_image).setImageResource(image)
        view.findViewById<TextView>(R.id.review_details_title).text = title
        view.findViewById<RatingBar>(R.id.review_details_rating).rating = rating
        view.findViewById<Button>(R.id.review_details_button).setOnClickListener{openDialog(it)}

        return view
    }

    private fun openDialog(it: View?) {
        val activity = it?.context as AppCompatActivity
        val dialog = ReviewDialog()
        dialog.show(activity.supportFragmentManager,"test tag")
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ReviewDetailsFragment()
    }
}