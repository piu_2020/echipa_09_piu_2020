package com.example.proiect_09_piu.ui.theatre

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.theatre.data.CardDatabase
import com.example.proiect_09_piu.ui.theatre.adapter.CardAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TheatreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TheatreFragment : Fragment() {
    var adapter: CardAdapter? = null
    private lateinit var recyclerRef: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerRef = view.findViewById(R.id.theatre_fragment_recycler)!!
        adapter = CardAdapter(requireContext(), CardDatabase().plays)
        recyclerRef.adapter = adapter
        val linearLayoutManager = LinearLayoutManager(view.context)
        recyclerRef.layoutManager = linearLayoutManager

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_theatre, container, false)

        recyclerRef = view.findViewById(R.id.theatre_fragment_recycler)
        recyclerRef.layoutManager = LinearLayoutManager(view.context);
        recyclerRef.adapter = CardAdapter(requireContext(), CardDatabase().plays)

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TheatreFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TheatreFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}