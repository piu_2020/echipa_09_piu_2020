package com.example.proiect_09_piu.ui.theatre.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.theatre.data.CardData

class CardAdapter(private val context: Context, private var dataList: ArrayList<CardData>) : RecyclerView.Adapter<CardViewHolder>() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val view = inflater.inflate(R.layout.generic_card_item,parent,false)
        return CardViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.bindData(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun changeList(newList : ArrayList<CardData>){
        this.dataList = newList
    }

    fun getCompany(index:Int) : CardData {
        return dataList[index]
    }
}