package com.example.proiect_09_piu.ui.publictransport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.MultiAutoCompleteTextView
import android.widget.MultiAutoCompleteTextView.CommaTokenizer
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText


class PublicTransportFragment : Fragment() {

    private lateinit var location: AutoCompleteTextView
    private lateinit var destinatie:AutoCompleteTextView
    private lateinit var lines: AutoCompleteTextView
    private lateinit var button: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_public_transport, container, false)

        location=view.findViewById(R.id.public_transport_location)
        destinatie=view.findViewById(R.id.public_transport_destination)
        lines=view.findViewById(R.id.public_transport_line)
        button=view.findViewById(R.id.public_transport_button)

        val items = listOf("10", "11", "12", "13")
        var adapter = ArrayAdapter(requireContext(), R.layout.list_item_1, items)
        lines.setAdapter(adapter)

        val locations= listOf("Locatia curenta","Str. Calea Victoriei")

        val adapter2= ArrayAdapter(requireContext(), R.layout.list_item_1, locations)
        location.setAdapter(adapter2)

        val destinations= listOf("Str. Constructorilor","Str. Rapsodiei","Str. Mihai Eminescu")

        val adapter3= ArrayAdapter(requireContext(), R.layout.list_item_1, destinations)
        destinatie.setAdapter(adapter3)


        location.doOnTextChanged { text, _, _, _ ->
            destinatie.doOnTextChanged {text2, _, _, _->
                if (text != "" && text2!="") {
                    if (location.text.toString() == "Locatia curenta" ){

                        when {
                            destinatie.text.toString() == "Str. Constructorilor" -> {
                                adapter = ArrayAdapter(requireContext(), R.layout.list_item_1, listOf("10"))
                                lines.setAdapter(adapter)
                            }
                            destinatie.text.toString() =="Str. Rapsodiei" -> {
                                adapter = ArrayAdapter(requireContext(), R.layout.list_item_1, listOf("13"))
                                lines.setAdapter(adapter)
                            }
                            destinatie.text.toString() == "Str. Mihai Eminescu" -> {
                                lines.setText("")
                                MaterialAlertDialogBuilder(requireContext())
                                    .setMessage("Nu s-a gasit nici o ruta pentru destinatia aleasa!")
                                    .setPositiveButton("OK") { dialog, which ->
                                        // Respond to positive button press

                                    }
                                    .show()
                            }
                        }
                    }
                    else if(location.text.toString() == "Str. Calea Victoriei" ){
                        when{
                            destinatie.text.toString() == "Str. Constructorilor" -> {
                                adapter = ArrayAdapter(requireContext(), R.layout.list_item_1, listOf("12"))
                                lines.setAdapter(adapter)
                            }
                        }
                    }
                }
            }
        }

        button.setOnClickListener{
            when{
                lines.text.toString()=="10"->{
                    MaterialAlertDialogBuilder(requireContext())
                        .setMessage("L-V:" +"\n"+"10:00, 10:30, 11:00, 11:30, 12:00, 12:30, 13:00, 13:30, 14:00"+"\n"
                                +"S-D"+"\n"+"10:00, 11:00, 12:00, 13:00, 14:00")
                        .setPositiveButton("OK") { dialog, which ->
                            // Respond to positive button press
                        }
                        .show()
                }
                lines.text.toString()=="11"->{
                    MaterialAlertDialogBuilder(requireContext())
                        .setMessage("L-V:" +"\n"+"08:00, 08:30, 09:00, 09:30, 10:00, 10:30, 11:00, 11:30, 12:00, 12:30, 13:00, 13:30, 14:00, 14:30, 15:00, 15:30, 16:00"+"\n"
                                +"S-D"+"\n"+"08:00, 09:00, 10:00, 11:00, 12:00, 13:00, 14:00, 15:00, 16:00, 17:00")
                        .setPositiveButton("OK") { dialog, which ->
                            // Respond to positive button press
                        }
                        .show()
                }
            }
        }

        return view
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            PublicTransportFragment()
    }
}

