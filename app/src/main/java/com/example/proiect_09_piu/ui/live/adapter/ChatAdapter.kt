package com.example.proiect_09_piu.ui.live.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.live.messages.Message
import com.example.proiect_09_piu.ui.live.viewholders.ChatViewHolder


class ChatAdapter(private val context: Context, private val dataSource: ArrayList<Message>): RecyclerView.Adapter<ChatViewHolder>() {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val view = inflater.inflate(R.layout.chat_message, parent, false)
        return ChatViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bindData(dataSource[position])
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }


    fun addItem(index: Int, item: Message) {
        dataSource.add(index, item)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}