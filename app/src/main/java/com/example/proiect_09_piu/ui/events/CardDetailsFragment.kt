package com.example.proiect_09_piu.ui.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.events.data.CardDetails


class CardDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_card_details, container, false)!!
        val imageId = CardDetails.currentSelection?.image!!
        val title = CardDetails.currentSelection?.title!!
        val date = CardDetails.currentSelection?.date!!

        view.findViewById<ImageView>(R.id.card_details_image).setImageResource(imageId)
        view.findViewById<TextView>(R.id.card_details_title).text = title
        view.findViewById<TextView>(R.id.card_details_date).text = date

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CardDetailsFragment()
    }
}