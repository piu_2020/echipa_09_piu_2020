package com.example.proiect_09_piu.ui.theatre.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.theatre.data.CardData
import com.example.proiect_09_piu.ui.theatre.data.CardDetails

class CardViewHolder(private var view: View) : RecyclerView.ViewHolder(view), View.OnClickListener{


    private lateinit var cardImageView: ImageView
    private lateinit var cardTtitleView: TextView
    private lateinit var cardDateView: TextView
    private var cardData : CardData? = null

    init {
        cardImageView = view.findViewById(R.id.generic_card_image)
        cardTtitleView = view.findViewById(R.id.generic_card_title)
        cardDateView = view.findViewById(R.id.generic_card_date)
        view.setOnClickListener(this)
    }


    fun bindData(data: CardData) {
        cardData = data
        cardImageView.setImageResource(data.image)
        cardTtitleView.text = data.title
        cardDateView.text = data.date
    }


    override fun onClick(v: View?) {
        CardDetails.currentSelection = cardData
        print (cardData)
        if (v != null) {
            val activity = view.context as AppCompatActivity
            val navController = activity.findNavController(R.id.nav_host_fragment)
            navController.navigate(R.id.nav_play_details)
        }
    }

}