package com.example.proiect_09_piu.ui.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.adapter.ReviewAdapter
import com.example.proiect_09_piu.ui.review.data.ReviewDatabase


class ReviewFragment : Fragment() {

    private lateinit var recyclerRef: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_review, container, false)!!

        recyclerRef = view.findViewById(R.id.review_fragment_recycler);
        recyclerRef.layoutManager = LinearLayoutManager(view.context);
        recyclerRef.adapter = ReviewAdapter(view.context, ReviewDatabase().reviews);

        return view

    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ReviewFragment()
    }
}