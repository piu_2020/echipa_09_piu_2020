package com.example.proiect_09_piu.ui.parking

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ParkingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ParkingFragment : Fragment() {
    private lateinit var textZona: AutoCompleteTextView
    private lateinit var textInmatriculate: TextInputEditText
    private lateinit var textOre: AutoCompleteTextView

    private lateinit var zonaLayout: TextInputLayout
    private lateinit var inmatriculareLayout: TextInputLayout
    private lateinit var oreLayout: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view =  inflater.inflate(R.layout.fragment_parking, container, false)

        val itemsZona = listOf("A1", "A2", "A3", "A4", "A5")
        val itemsOre = listOf("0.5", "1", "2", "3", "6", "12", "24")

        val dropDownZona = view.findViewById<AutoCompleteTextView>(R.id.parking_dropdown_zona_text)
        val dropDownOre = view.findViewById<AutoCompleteTextView>(R.id.parking_dropdown_ore_text)

        val adapterZona = ArrayAdapter(requireContext(), R.layout.dropdown_item, itemsZona)
        val adapterOre = ArrayAdapter(requireContext(), R.layout.dropdown_item, itemsOre)

        dropDownZona?.setAdapter(adapterZona)
        dropDownOre?.setAdapter(adapterOre)

        textZona = view.findViewById(R.id.parking_dropdown_zona_text)
        textInmatriculate = view.findViewById(R.id.parking_textField_inmatriculare)
        textOre = view.findViewById(R.id.parking_dropdown_ore_text)

        zonaLayout = view.findViewById(R.id.parking_dropdown_zona)
        inmatriculareLayout = view.findViewById(R.id.parking_textField_inmatriculare_layout)
        oreLayout = view.findViewById(R.id.parking_dropdown_ore)

        textZona.doOnTextChanged { text, start, before, count ->  zonaLayout.isErrorEnabled = false}
        textInmatriculate.doOnTextChanged { text, start, before, count -> inmatriculareLayout.isErrorEnabled = false}
        textOre.doOnTextChanged { text, start, before, count -> oreLayout.isErrorEnabled = false }

        val button: Button = view.findViewById(R.id.parking_payButton) as Button
        button.setOnClickListener(View.OnClickListener {
            handlePressButton(view)
        })

        return view;
    }

    fun validateInput(view: View) : Boolean {
        var valid = true;
        val textZona = getView()?.findViewById<AutoCompleteTextView>(R.id.parking_dropdown_zona_text)
        val textInmatriculare = getView()?.findViewById<TextInputEditText>(R.id.parking_textField_inmatriculare)
        val textOre = getView()?.findViewById<AutoCompleteTextView>(R.id.parking_dropdown_ore_text)

        if (textZona?.text.isNullOrBlank()) {
            zonaLayout.isErrorEnabled = true
            zonaLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        if (textInmatriculare?.text.isNullOrBlank()) {
            inmatriculareLayout.isErrorEnabled = true
            inmatriculareLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        if (textOre?.text.isNullOrBlank()) {
            oreLayout.isErrorEnabled = true
            oreLayout.error = "Acest camp este obligatoriu!"
            valid = false;
        }

        return valid;

    }

    fun handlePressButton(view: View) {
        if (validateInput(view)) {
            //pentru prezentare
            //val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.maiestiprost.com"))
            //pentru cand se uita Domnul Profesor de la curs
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plataonline.com"))
            startActivity(browserIntent)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ParkingFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ParkingFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}