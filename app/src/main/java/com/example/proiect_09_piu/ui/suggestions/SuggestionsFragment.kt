package com.example.proiect_09_piu.ui.suggestions

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.ProgressBar
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.concurrent.schedule

class SuggestionsFragment : Fragment() {


    private lateinit var field: AutoCompleteTextView;
    private lateinit var suggestionField: TextInputEditText;
    private lateinit var submitButton: Button;
    private lateinit var spinner: ProgressBar;

    private var categorySelected: Boolean = false;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_suggestions, container, false)

        field = view.findViewById(R.id.category_field)!!

        val items = listOf("Infrastructura", "Transport public", "Salubritare", "Spatii publice")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
        field.setAdapter(adapter)

        submitButton = view.findViewById(R.id.submit_suggestion_button)
        spinner = view.findViewById(R.id.action_bar_spinner)
        suggestionField = view.findViewById(R.id.suggestion_input_field)

        val layoutInput = view.findViewById(R.id.suggestion_input_layout) as TextInputLayout;
        val layoutDropdown = view.findViewById(R.id.suggestion_category_dropdown) as TextInputLayout;

        suggestionField.doOnTextChanged { text, start, before, count ->  layoutInput.isErrorEnabled = false;}

        field.doOnTextChanged { text, start, before, count ->  layoutDropdown.isErrorEnabled = false;}


        val alertDialog = AlertDialog.Builder(this.context)
        alertDialog.setTitle("Sugestia dumneavoastra a fost inregistrata! Va multumim!")
        alertDialog.setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))

        submitButton.setOnClickListener {

            if(suggestionField.text.toString().isEmpty()) {
                layoutInput.isErrorEnabled = true
                layoutInput.error = "Acest camp este obligatoriu!"
            }

            if (field.text.toString().isEmpty()) {
                layoutDropdown.isErrorEnabled = true
                layoutDropdown.error = "Acest camp este obligatoriu!"
            }

            if(!field.text.toString().isEmpty()&& !suggestionField.text.toString().isEmpty()){
                spinner.visibility = View.VISIBLE
                Timer("Spinner", false).schedule(2000) {
                    Handler(Looper.getMainLooper()).post {
                        spinner.visibility = View.GONE
                        alertDialog.show()
                    }
                }
            }
        }

        return view
    }

    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
            field.setText("");
            suggestionField.setText("");
    }
}