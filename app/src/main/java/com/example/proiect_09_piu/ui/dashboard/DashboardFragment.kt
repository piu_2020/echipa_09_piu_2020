package com.example.proiect_09_piu.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.dashboard.layoutDecorator.DotsIndicatorDecoration
import com.example.proiect_09_piu.ui.events.adapter.CardAdapter
import com.example.proiect_09_piu.ui.events.data.CardDatabase
import com.example.proiect_09_piu.ui.review.adapter.ReviewAdapter
import com.example.proiect_09_piu.ui.review.data.ReviewDatabase


class DashboardFragment : Fragment() {

    private lateinit var newsRecyclerRef: RecyclerView
    private lateinit var eventsRecyclerRef: RecyclerView
    private lateinit var reviewsRecyclerRef: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)!!

        val layoutManager1 = LinearLayoutManager(
            view.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        val layoutManager2 = LinearLayoutManager(
            view.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        val layoutManager3 = LinearLayoutManager(
            view.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        newsRecyclerRef = view.findViewById(R.id.dashboard_news_recycler);
        eventsRecyclerRef = view.findViewById(R.id.dashboard_events_recycler);
        reviewsRecyclerRef = view.findViewById(R.id.dashboard_reviews_recycler);

        newsRecyclerRef.layoutManager = layoutManager1
        eventsRecyclerRef.layoutManager = layoutManager2
        reviewsRecyclerRef.layoutManager = layoutManager3

        newsRecyclerRef.adapter = CardAdapter(view.context, CardDatabase().news);
        eventsRecyclerRef.adapter = CardAdapter(view.context, CardDatabase().events);
        reviewsRecyclerRef.adapter = ReviewAdapter(view.context, ReviewDatabase().reviews);

        val color = ContextCompat.getColor(requireContext(), R.color.white)
        newsRecyclerRef.addItemDecoration(
            DotsIndicatorDecoration(
                10,
                60,
                100,
                color,
                color
            )
        )

        eventsRecyclerRef.addItemDecoration(
            DotsIndicatorDecoration(
                10,
                60,
                100,
                color,
                color
            )
        )

        reviewsRecyclerRef.addItemDecoration(
            DotsIndicatorDecoration(
                10,
                60,
                100,
                color,
                color
            )
        )

        return view
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            DashboardFragment()
    }
}