package com.example.proiect_09_piu.ui.voting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.proiect_09_piu.R
import com.google.android.material.textfield.TextInputEditText


class VotingFragment : Fragment() {

    private lateinit var address: AutoCompleteTextView
    private lateinit var section: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_voting, container, false)
        address=view.findViewById(R.id.voting_address)
        section=view.findViewById(R.id.voting_section)


        val adrese= listOf("Str. Constructorilor","Str. Rapsodiei","Str. Mihai Eminescu")
        val adapter= ArrayAdapter(requireContext(), R.layout.list_item_1, adrese)
        address.setAdapter(adapter)

        address.doOnTextChanged { text, _, _, _ ->
            if (text != "") {
                when {
                    address.text.toString() == "Str. Constructorilor" -> {
                        section.setText("Nr.11 Scoala Generala \"Mihai Viteazu\"")
                    }
                    address.text.toString() == "Str. Rapsodiei" -> {
                        section.setText("Nr.12 Colegiul National \"Mihai Eminescu\" ")
                    }
                    address.text.toString() == "Str. Mihai Eminescu" -> {
                        section.setText("Nr.13 Colegiul National \"Liviu Rebreanu\" ")
                    }
                }
            }
        }

        return view
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            VotingFragment()
    }
}