package com.example.proiect_09_piu.ui.review.data

object UserReviews {

    var hashMap : HashMap<String, ArrayList<UserReview>>
            = HashMap<String, ArrayList<UserReview>> ()


    init{
        var park1List = ArrayList<UserReview>()
        park1List.add(UserReview("gicu",4.0f,"Frumos, elegant"))
        park1List.add(UserReview("gigel",3.0f,"Este meh"))
        hashMap["Parcul Central"] = park1List

        var park2List = ArrayList<UserReview>()
        park2List.add(UserReview("ion",5.0f,"super!"))
        park2List.add(UserReview("vasile",1.0f,"nasol..."))
        hashMap["Parcul Iulius"] = park2List

        var park3List = ArrayList<UserReview>()
        park3List.add(UserReview("alex",2.8f,"peste medie"))
        park3List.add(UserReview("ius",4.3f,"frumos"))
        hashMap["Parcul Cetățuia"] = park3List
    }

}