package com.example.proiect_09_piu.ui.events.utils

import com.example.proiect_09_piu.ui.events.data.CardData
import java.util.*
import kotlin.Comparator

class SearchKeyWordComparator(private val keywords: String) : Comparator<CardData> {

    private fun getHits(input: CardData): Int {
        var hits = 0
        val keyword_split = keywords.split(" ").map { it.toLowerCase(Locale.ROOT) }
        val input_split = input.title.split(" ").map { it.toLowerCase(Locale.ROOT) }

        for (s in keyword_split) {
            if (input_split.contains(s))
                hits += 1
        }
        return hits
    }

    override fun compare(o1: CardData?, o2: CardData?): Int {
        val o1_hits = getHits(o1!!)
        val o2_hits = getHits(o2!!)
        return when {
            o1_hits == o2_hits -> 0
            o1_hits < o2_hits -> 1
            else -> -1
        }
    }
}