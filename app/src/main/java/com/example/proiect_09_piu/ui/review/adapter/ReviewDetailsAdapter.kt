package com.example.proiect_09_piu.ui.review.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.data.UserReview

class ReviewDetailsAdapter(private val context: Context, private var dataList: ArrayList<UserReview>) : RecyclerView.Adapter<ReviewDetailsViewHolder>() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewDetailsViewHolder {
        val view = inflater.inflate(R.layout.review_user_item,parent,false)
        return ReviewDetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReviewDetailsViewHolder, position: Int) {
        holder.bindData(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun changeList(newList : ArrayList<UserReview>){
        this.dataList = newList
    }

    fun getCompany(index:Int) : UserReview {
        return dataList[index]
    }
}