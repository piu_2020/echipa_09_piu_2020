package com.example.proiect_09_piu.ui.review.adapter

import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.data.Review
import com.example.proiect_09_piu.ui.review.data.ReviewSelection

class ReviewViewHolder(private var view: View) : RecyclerView.ViewHolder(view), View.OnClickListener{


    private lateinit var reviewImageView: ImageView
    private lateinit var reviewTitleView: TextView
    private lateinit var reviewRatingView: RatingBar

    private var reviewData : Review? = null

    init {
        reviewImageView = view.findViewById(R.id.review_card_image)
        reviewTitleView = view.findViewById(R.id.review_card_title)
        reviewRatingView = view.findViewById(R.id.review_card_rating)
        view.setOnClickListener(this)
    }


    fun bindData(data: Review) {
        reviewData = data
        reviewImageView.setImageResource(data.image)
        reviewTitleView.text = data.title
        reviewRatingView.rating = data.rating
    }


    override fun onClick(v: View?) {
        ReviewSelection.current = reviewData

        if (v != null) {

            val activity = view.context as AppCompatActivity
            val navController = activity.findNavController(R.id.nav_host_fragment)
            navController.navigate(R.id.nav_review_details)


            /*
            val activity = view.context as AppCompatActivity

            val dialog = ReviewDialog()
            dialog.show(activity.supportFragmentManager,"test tag")
             */

        }
    }
}