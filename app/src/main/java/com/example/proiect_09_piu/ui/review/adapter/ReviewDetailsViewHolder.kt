package com.example.proiect_09_piu.ui.review.adapter

import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.review.data.UserReview

class ReviewDetailsViewHolder(private var view: View) : RecyclerView.ViewHolder(view){


    private lateinit var reviewDetailsUsernameView : TextView
    private lateinit var reviewDetailsCommentView: TextView
    private lateinit var reviewDetailsRatingView: RatingBar


    init {
        reviewDetailsUsernameView = view.findViewById(R.id.review_item_username)
        reviewDetailsCommentView = view.findViewById(R.id.review_item_comment)
        reviewDetailsRatingView = view.findViewById(R.id.review_item_rating)
    }


    fun bindData(data: UserReview) {
        reviewDetailsUsernameView.text = data.username
        reviewDetailsCommentView.text = data.comment
        reviewDetailsRatingView.rating = data.rating
    }


}