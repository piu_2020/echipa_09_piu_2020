package com.example.proiect_09_piu.ui.events


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.ui.events.adapter.CardAdapter
import com.example.proiect_09_piu.ui.events.data.CardDatabase
import com.example.proiect_09_piu.ui.events.utils.SearchKeyWordComparator
import com.google.android.material.textfield.TextInputEditText
import java.util.*


class EventsFragment : Fragment() {

    private lateinit var recyclerRef: RecyclerView
    private lateinit var keywordsInput : TextInputEditText
    private lateinit var adapter : CardAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_events, container, false)

        recyclerRef = view.findViewById(R.id.events_fragment_recycler);
        recyclerRef.layoutManager = LinearLayoutManager(view.context);
        adapter = CardAdapter(view.context, CardDatabase().events)
        recyclerRef.adapter = adapter

        keywordsInput = view.findViewById(R.id.events_fragment_search)

        keywordsInput.doOnTextChanged { text, _, _, _ ->
            var list = CardDatabase().events
            if(text != "")
                Collections.sort(list,SearchKeyWordComparator(text.toString()))
            adapter.changeList(list)
            adapter.notifyDataSetChanged()
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            EventsFragment()
    }
}