package com.example.proiect_09_piu.ui.review.data

import com.example.proiect_09_piu.R

class ReviewDatabase {

    var reviews : ArrayList<Review>

    init{
        reviews = ArrayList<Review>()
        reviews.add(Review(R.drawable.park1_image,"Parcul Central",4.3f))
        reviews.add(Review(R.drawable.park2_image,"Parcul Iulius",3.5f))
        reviews.add(Review(R.drawable.park3_image,"Parcul Cetățuia",2.7f))
        updateRatings()
    }


    fun updateRatings() {
        for(r in reviews){
            val revList = UserReviews.hashMap[r.title]!!
            var rating = 0.0f
            for(userRev in revList){
                rating += userRev.rating
            }
            rating /= revList.size
            r.rating = rating
        }
    }
}