package com.example.proiect_09_piu.ui.review.dialog


import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.widget.doOnTextChanged
import com.example.proiect_09_piu.R
import com.example.proiect_09_piu.models.LoggedUser
import com.example.proiect_09_piu.ui.review.data.ReviewDatabase
import com.example.proiect_09_piu.ui.review.data.ReviewSelection
import com.example.proiect_09_piu.ui.review.data.UserReview
import com.example.proiect_09_piu.ui.review.data.UserReviews
import com.google.android.material.textfield.TextInputEditText


class ReviewDialog : AppCompatDialogFragment() {

    private lateinit var ratingBar : RatingBar
    private lateinit var commentInput : TextInputEditText
    private lateinit var submitButton : Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view: View = inflater.inflate(R.layout.review_dialog_layout, null)

        builder.setView(view)

        ratingBar = view.findViewById(R.id.review_dialog_rating)
        commentInput = view.findViewById(R.id.review_dialog_textField2)
        submitButton = view.findViewById(R.id.review_dialog_button)

        commentInput.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                if(text.length > 1){
                    submitButton.isEnabled = true
                }
            }
        }

        submitButton.isEnabled = false
        submitButton.setOnClickListener{submitReview(it)}

        return builder.create()

    }

    private fun submitReview(it: View?) {
        val newReview = UserReview(LoggedUser.username,ratingBar.rating,commentInput.text.toString())
        UserReviews.hashMap[ReviewSelection.current?.title]?.add(newReview)
        ReviewDatabase().updateRatings()
        this.dismiss()
    }


}